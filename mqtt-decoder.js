import {injectPlugin} from "@floocus/spider-plugin";
import toBuffer from 'blob-to-buffer';
import mqtt from 'mqtt-packet';
let React;
const opts = { protocolVersion: 4 }; // default is 4. Usually, opts is a connect packet
const parser = mqtt.parser(opts);

async function mqttDecoder({
   inputs: {payload}, //Blob
   parameters: {},
   callbacks: {setDecodedPayload, onShowInfo, onShowError, onShowWarning },
   libs: {React: R, Field, Section, Payload, Divider}
}){
    React = R;

    if(payload) {
        //get payload as Buffer
        toBuffer(payload, function (err, input) {
            if (err) throw err;
            const packets = [];
            try{
                // Synchronously emits all the parsed packets
                parser.on('packet', packet => packets.push(packet));
                parser.on('error', err => onShowWarning(err.message));
                const left = parser.parse(input);
                setDecodedPayload(
                    <>
                        {packets.map((p, i) => {
                            const payload = p.payload?.length && p.payload.toString();
                            let language;
                            try{
                                JSON.parse(payload);
                                language = 'json';
                            }
                            catch (e) {
                                language = 'text'
                            }

                            return (
                                <>
                                    {(p.protocolId !== undefined) &&
                                        <Section title={'Connection'}>
                                            {<Field header={'Protocol'}>{p.protocolId} v{p.protocolVersion}</Field>}
                                            {(p.keepalive !== undefined) && <Field header={'Keep alive'}>{p.keepalive}</Field>}
                                        </Section>
                                    }
                                    <Section title={'Command'}>

                                        {(p.topic !== undefined) && <Field header={'Topic'}>{p.topic}</Field>}
                                        <Field header={'Command'}>{p.cmd}{(p.messageId !== undefined) ? ` #${p.messageId}` : ''}</Field>
                                        {(p.returnCode !== undefined) && <Field header={'ReturnCode'}>{p.returnCode}</Field>}
                                        {(p.username !== undefined) && <Field header={'Username'}>{p.username}</Field>}
                                        {(p.password !== undefined) && <Field header={'Password'}>{p.password.toString()}</Field>}
                                        {(p.clientId !== undefined) && <Field header={'Client Id'}>{p.clientId}</Field>}
                                        {(p.subscriptions !== undefined) &&
                                            <Field header={'Subscriptions'}>
                                                {p.subscriptions.map((s, idx) =>
                                                        <span key={idx}>
                                                    {s.topic} (qos: {s.qos})
                                                            {idx < p.subscriptions.length - 1 && <br/>}
                                                </span>
                                                )}
                                            </Field>
                                        }
                                        {(p.granted !== undefined) && <Field header={'Granted'}>{JSON.stringify(p.granted)}</Field>}
                                        {(p.unsubscriptions !== undefined) && <Field header={'Unsubscriptions'}>{JSON.stringify(p.unsubscriptions)}</Field>}
                                        <Field header={'Metadata'}>QOS: {p.qos} - Duplicate: {p.dup ? '☑' : '□'} - Retain: {p.retain ? '☑' : '□'}</Field>
                                    </Section>
                                    {payload &&
                                        <Section title={'Payload'}>
                                            <Payload language={language} payload={payload}/>
                                        </Section>
                                    }
                                    {i < packets.length - 1 &&
                                        <>
                                            <br/>
                                            <Divider/>
                                        </>
                                    }
                                </>
                            )
                        })}
                    </>
                )
            }
            catch(e){
                onShowWarning(`Error parsing payload: ${e.message}`);
                console.log(e);
            }
        })
    }
}

injectPlugin({
    id: 'mqtt-decoder',
    type: 'tcp-payload-decode-plugin',
    version: '0.1',
    func: mqttDecoder,
    errorMessage: 'Could not decode payload!'
});